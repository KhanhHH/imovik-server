module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true
    },
    extends: 'eslint:recommended',
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parserOptions: {
        sourceType: 'module',
        ecmaVersion: 2018
    },
    rules: {
        indent: 'off',
        'linebreak-style': ['error', 'windows'],
        quotes: 'off',
        semi: ['error', 'always'],
        'no-unused-vars': 'off',
        'no-console': 'off'
    }
};
