const { app } = require('./config');

const bcrypt = require('bcrypt');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const {
    knex,
    createAccountOnDatabase,
    getAccountInfoFromDatabase
} = require('./knex');

const secretKey = 'imovik-secret-key';

const decodeToken = (headerAuth) => {
    const token = headerAuth.slice(
        7,
        headerAuth.length
    );
    const decodedToken = jwt.decode(token);
    return decodedToken;
};

const checkToken = (req, res, next) => {
    if (req.headers.authorization) {
        const token = req.headers.authorization.slice(
            7,
            req.headers.authorization.length
        );
        if (verifyToken(token) === true) {
            next();
            console.log('Authorized');
        } else {
            res.status(401).send('Invalid signature.');
        }
    }
};

const verifyToken = (token) => {
    return jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            return false;
        }
        return true;
    });
};

app.post('/account/create', (req, res) => {
    if (
        !req.body.display_name ||
        !req.body.email ||
        !req.body.password ||
        !req.body.verify_password
    ) {
        res.json({
            success: false,
            message: 'Bạn chưa nhập đủ thông tin!'
        });
    } else if (validator.isEmail(req.body.email)) {
        if (req.body.password === req.body.verify_password) {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                // Store hash in password DB.
                const new_account = {
                    display_name: req.body.display_name,
                    email: req.body.email,
                    password: hash,
                    balance: 0
                };

                createAccountOnDatabase(new_account)
                    .then(() => {
                        res.json({
                            success: true,
                            message: 'Tạo tài khoản thành công!'
                        });
                    })
                    .catch((err) => {
                        res.json({
                            success: false,
                            message: 'Email đã được sử dụng!'
                        });
                    });
            });
        } else {
            res.json({
                success: false,
                message: 'Mật khẩu không khớp'
            });
        }
    } else {
        res.json({
            success: false,
            message: 'Email không hợp lệ!'
        });
    }
});

app.post('/account/login', (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.json({
            success: false,
            message: 'Bạn chưa nhập đủ thông tin!'
        });
    } else {
        getAccountInfoFromDatabase(req.body.email)
            .then((data) => {
                bcrypt.compare(
                    req.body.password,
                    data[0].password,
                    (b_err, b_res) => {
                        if (b_res === true) {
                            const token = jwt.sign(
                                {
                                    id: data[0].id,
                                    email: data[0].email,
                                    display_name: data[0].display_name,
                                    balance: data[0].balance,
                                },
                                secretKey,
                                { expiresIn: '12h' }
                            );
                            res.json({
                                success: true,
                                message: 'Xác  thực thành công!',
                                token: token
                            });
                        } else {
                            res.json({
                                success: false,
                                message: 'Email và mật khẩu không khớp!'
                            });
                        }
                    }
                );
            })
            .catch((err) => {
                res.json({
                    success: false,
                    message: 'Email và mật khẩu không khớp!'
                });
            });
    }
});

exports.checkToken = checkToken;
exports.decodeToken = decodeToken;
