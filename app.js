const { app } = require('./config');
const { knex } = require('./knex');
const port = 3000;

const fetch = require('./fetch');
const auth = require('./auth');

const banner = require('./controllers/banner');
const movie = require('./controllers/movie');
const schedule = require('./controllers/schedule');
const booking = require('./controllers/booking');
const payment = require('./controllers/payment');
const account = require('./controllers/account');
const cinema = require('./controllers/cinema');


app.get('/', (req, res) => {
    console.log('APP WORKED');
    res.send('Hello World! ayyyy');
});

app.get('/home/banner', (req, res) => {
    banner.handleBanner(req, res, knex);
});

app.get('/movie/list', (req, res) => {
    movie.handleMovieList(req, res, knex);
});

app.post('/movie/create', (req, res) => {
    movie.handleMovieCreate(req, res, knex);
});

app.get('/movie/detail/:id', (req, res) => {
    movie.handleMovieDetail(req, res, knex);
});

app.post('/schedule/create', (req, res) => {
    schedule.handleScheduleCreate(req, res, knex);
});

app.post('/schedule/list/', (req, res) => {
    schedule.handleScheduleList(req, res, knex);
});

app.post('/schedule/list/date', (req, res) => {
    schedule.handleScheduleListDate(req, res, knex);
});

app.post('/booking/seats/create', (req, res) => {
    booking.handleBookingSeatsCreate(req, res, knex);
});

app.get('/booking/seats/:id', (req, res) => {
    booking.handleBookingSeatsId(req, res, knex);
});

app.post('/payment/booking', auth.checkToken, (req, res) => {
    payment.handlePaymentBooking(req, res, knex);
});

app.post('/payment/top-up', auth.checkToken, (req, res) => {
    payment.handlePaymentTopup(req, res, knex, auth);
});

app.get('/account/balance', auth.checkToken, (req, res) => {
    account.handleAccountBalance(req, res, knex, auth);
});

app.get('/account/booking-history/list', auth.checkToken, (req, res) => {
    account.handleAccountBookingHistoryList(req, res, knex, auth);
});

app.get('/account/booking-history/detail/:id', auth.checkToken, (req, res) => {
    account.handleAccountBookingHistoryDetail(req, res, knex, auth);
});

app.post('/cinema/create', (req, res) => {
    cinema.handleCinemaCreate(req, res, knex);
});

app.get('/cinema/list', (req, res) => {
    cinema.handleCinemaList(req, res, knex);
});

app.listen(port, () => console.log(`Port: ${port}`));
