const { XMLHttpRequest } = require('xmlhttprequest');
const { ajax } = require('rxjs/ajax');

const createXHR = () => {
    return new XMLHttpRequest();
};

const fecthAPI = (url, keyCache) =>
    ajax({
        createXHR,
        headers: {
            'Content-Type': 'application/json'
        },
        url: 'https://www.123phim.vn/apitomapp',
        method: 'POST',
        body: {
            param: {
                url: url,
                keyCache: keyCache
            },
            method: 'GET'
        }
    });

const fetchPremieringFilmList = () => {
    return fecthAPI('/film/list?status=2', 'showing-film');
};
const fetchUpcomingFilmList = () => {
    return fecthAPI('/film/list?status=1', 'main-films-coming');
};
const fetchMovieDetail = (film_id) => {
    return fecthAPI(
        '/film/detail?film_id=' + film_id,
        'movie-detail' + film_id
    );
};

const fetchFilmScheduleDetail = (film_id) => {
    const today = new Date();
    const start_date =
        today.getFullYear() +
        '-' +
        (today.getMonth() + 1) +
        '-' +
        today.getDate();
    const week = new Date();
    week.setDate(week.getDate() + 7);

    const end_date =
        week.getFullYear() + '-' + (week.getMonth() + 1) + '-' + week.getDate();

    return fecthAPI(
        '/session/film?cinema_id=-1&film_id=' +
            film_id +
            '&start_date=' +
            start_date +
            '&end_date=' +
            end_date +
            '&location_id=1',
        'no-cache'
    );
};

const fetchBannerList = () => {
    return fecthAPI('/film/banner', 'home-banner');
};

// exports.fetchPremieringFilmList = fetchPremieringFilmList;
// exports.fetchUpcomingFilmList = fetchUpcomingFilmList;
// exports.fetchMovieDetail = fetchMovieDetail;
// exports.fetchFilmScheduleDetail = fetchFilmScheduleDetail;

module.exports = {
    fetchPremieringFilmList,
    fetchUpcomingFilmList,
    fetchMovieDetail,
    fetchFilmScheduleDetail,
    fetchBannerList
};

