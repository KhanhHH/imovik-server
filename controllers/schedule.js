const { from } = require('rxjs');
const { mergeMap, toArray } = require('rxjs/operators');

const handleScheduleCreate = (req, res, knex) => {
    knex('schedules')
        .insert(req.body)
        .then(() => {
            res.status(200).send('[SERVER] CREATED SCHEDULE');
            console.log('[SERVER] INSERTED DATA');
            console.log(req.body);
        })
        .catch((err) => {
            console.log(err);
        });
};

const handleScheduleList = (req, res, knex) => {
    knex.select()
        .distinct(
            'date',
            'cinema_group',
            'film_id',
            'theater_id',
            'theater_name'
        )
        .from('schedules')
        .where({
            film_id: req.body.film_id,
            cinema_group: req.body.cinema_group,
            date: req.body.date
        })
        .orderBy('theater_id')
        .then((knex_data) => {
            from(knex_data)
                .pipe(
                    mergeMap((data) => {
                        return getScheduleTime(data).then((schedule_data) => {
                            const new_schedule = {
                                date: data.date,
                                cinema_group: data.cinema_group,
                                film_id: data.film_id,
                                theater_id: data.theater_id,
                                theater_name: data.theater_name,
                                time_details: schedule_data
                            };
                            return new_schedule;
                        });
                    }),
                    toArray()
                )

                .subscribe((data) => {
                    console.log('[SERVER] SENT DATA: ', data);
                    res.status(200).send(data);
                });
        })
        .catch((err) => {
            console.log(err);
        });

    const getScheduleTime = (schedule_details) => {
        return knex
            .select('time', 'time_id')
            .from('schedules')
            .where({
                film_id: schedule_details.film_id,
                cinema_group: schedule_details.cinema_group,
                date: schedule_details.date,
                theater_id: schedule_details.theater_id
            })
            .orderBy('time');
    };
};

const handleScheduleListDate = (req, res, knex) => {
    knex.select('date')
        .from('schedules')
        .where({
            film_id: req.body.film_id
        })
        .then((data) => {
            console.log('[SERVER] SENT DATA: ', data);
            res.status(200).send(data);
        })
        .catch((err) => {
            console.log(err);
        });
};


module.exports = {
    handleScheduleCreate,
    handleScheduleList,
    handleScheduleListDate
};
