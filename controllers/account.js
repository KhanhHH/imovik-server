const handleAccountBalance = (req, res, knex, auth) => {
    const userInfo = auth.decodeToken(req.headers.authorization);
    knex.select('balance')
        .from('users')
        .where('id', userInfo.id)
        .then((data) => {
            res.json({
                result: data[0],
                success: true
            });
        });
};

const handleAccountBookingHistoryList = (req, res, knex, auth) => {
    const userInfo = auth.decodeToken(req.headers.authorization);
    knex.select(
        'payment_booking.id',
        'movies.title',
        'payment_booking.purchase_date',
        'payment_booking.price'
    )
        .from(knex.raw('payment_booking, movies'))
        .where(
            knex.raw(
                'payment_booking.film_id = movies.film_id AND payment_booking.user_id = ' +
                    userInfo.id
            )
        )
        .orderBy('payment_booking.id', 'desc')
        .then((data) => {
            res.send(data);
            console.log('[SERVER] SENT DATA: ', data);
        })
        .catch((err) => {
            console.log(err);
        });
};

const handleAccountBookingHistoryDetail = (req, res, knex, auth) => {
    const userInfo = auth.decodeToken(req.headers.authorization);
    knex.select(
        'movies.title',
        'movies.poster_img',
        'payment_booking.purchase_date',
        'payment_booking.price',
        'payment_booking.seats',
        'schedules.time',
        'schedules.date',
        'payment_booking.ticket_code'
    )
        .from(knex.raw('payment_booking, movies, schedules'))
        .where(
            knex.raw(
                'payment_booking.film_id = movies.film_id AND payment_booking.time_id = schedules.time_id AND payment_booking.user_id = ' +
                    userInfo.id +
                    ' AND payment_booking.id = ' +
                    req.params.id
            )
        )
        .then((data) => {
            if(data[0]){
                // res.send(data[0]);
                res.json({
                    result: data[0],
                    success: true,
                    message: 'Thành công!'
                });
                console.log('[SERVER] SENT DATA: ', data[0]);
            } else {
                res.json({
                    success: false,
                    message: 'Mã đổi vé của bạn đã hết hạn!'
                });
            }

        })
        .catch((err) => {
            console.log(err);
        });
};

module.exports = {
    handleAccountBalance,
    handleAccountBookingHistoryList,
    handleAccountBookingHistoryDetail
};

