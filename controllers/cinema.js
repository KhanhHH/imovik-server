const handleCinemaCreate = (req, res, knex) => {
    knex('cinemas')
        .insert(req.body)
        .then(() => {
            res.status(200).send('[SERVER] CREATED CINEMA');
            console.log('[SERVER] INSERTED DATA', req.body);
        })
        .catch((err) => {
            console.log(err);
        });
};

const handleCinemaList = (req, res, knex) => {
    knex.select()
        .from('cinemas')
        .orderBy('group')
        .then((data) => {
            res.status(200).send(data);
            console.log('[SERVER] SENT DATA: ', data);
        })
        .catch((err) => {
            console.log(err);
        });
};

module.exports = {
    handleCinemaCreate,
    handleCinemaList
};

