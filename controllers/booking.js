const { from } = require('rxjs');
const { mergeMap, mergeAll, toArray } = require('rxjs/operators');

const handleBookingSeatsCreate = (req, res, knex) => {
    const seats = req.body.seats;
    from(seats)
        .pipe(
            mergeMap((data) => {
                return data.col.map((c) => {
                    const new_seat = {
                        seat_id: c.id,
                        row: data.row,
                        available: c.available,
                        time_id: req.body.time_id
                    };
                    return new_seat;
                });
            })
        )
        .subscribe((data) => {
            knex('seats')
                .insert(data)
                .then(() => {
                    console.log('[SERVER] INSERTED DATA');
                    console.log(data);
                })
                .catch((err) => {
                    console.log(err);
                });
        });
};
const handleBookingSeatsId = (req, res, knex) => {
    const seats = knex
        .select()
        .distinct('row')
        .from('seats')
        .where('time_id', req.params.id)
        .orderBy('row');
    from(seats)
        .pipe(
            mergeAll(),
            mergeMap((data) => {
                return getSeatIdByRow(data.row, req.params.id).then((s) => {
                    const seat = {
                        row: data.row,
                        col: s
                    };
                    return seat;
                });
            }),
            toArray()
        )
        .subscribe((data) => {
            console.log('[SERVER] SENT DATA: ', data);
            res.send(data);
        });
    const getSeatIdByRow = (row, time_id) => {
        return knex
            .select('seat_id', 'available')
            .from('seats')
            .where({
                row: row,
                time_id: time_id
            })
            .orderBy(['row', 'seat_id']);
    };
};

module.exports = {
    handleBookingSeatsCreate,
    handleBookingSeatsId
};
