const nanoid = require('nanoid/generate');
const { from } = require('rxjs');
const { mergeMap, mergeAll, every } = require('rxjs/operators');

const handlePaymentBooking = (req, res, knex) => {
    const paymentInfo = {
        user_id: req.body.user_id,
        time_id: req.body.time_id,
        film_id: req.body.film_id,
        price: req.body.price,
        seats: req.body.seats,
        ticket_code: nanoid('1234567890ABCDEFGHJKLQWERTYUIOPZXCVBNM', 10),
        purchase_date: new Date().toISOString()
    };

    const getAccountBalance = (user_id) => {
        return knex
            .select('balance')
            .from('users')
            .where('id', user_id);
    };

    const getSeatInfoByRowSeatId = (row, seat_id) => {
        return knex
            .select()
            .from('seats')
            .where({
                time_id: paymentInfo.time_id,
                row: row,
                seat_id: seat_id
            });
    };

    const insertPaymentBooking = (payment_info) => {
        return knex('payment_booking')
            .returning('id')
            .insert(payment_info);
    };

    const updateSeats = (seat) => {
        knex('seats')
            .insert({
                time_id: paymentInfo.time_id,
                row: seat.row,
                seat_id: seat.seat_id,
                available: false
            })
            .then((data) => {
                console.log('[DATABASE] UPDATED SEAT', seat);
            });
    };

    getAccountBalance(paymentInfo.user_id).then((data) => {
        if (data[0].balance > paymentInfo.price) {
            payment();
        } else {
            res.json({
                success: false,
                message:
                    // '[SERVER] INSUFFICIENT BALANCE!'
                    'Số dư trong tài khoản không đủ!'
            });
        }
    });

    const payment = () => {
        if (paymentInfo.seats.length !== 0) {
            from(paymentInfo.seats)
                .pipe(
                    mergeMap((data) =>
                        getSeatInfoByRowSeatId(data.row, data.seat_id)
                    ),
                    mergeAll(),
                    every((data) => data.available === true)
                )
                .subscribe((data) => {
                    if (data === true) {
                        insertPaymentBooking(paymentInfo)
                            .then((data) => {
                                paymentInfo.seats.map((seat) =>
                                    updateSeats(seat)
                                );
                                getAccountBalance(paymentInfo.user_id).then(
                                    (db) => {
                                        const afterBookingBalance =
                                            db[0].balance - paymentInfo.price;
                                        knex('users')
                                            .where('id', paymentInfo.user_id)
                                            .update(
                                                'balance',
                                                afterBookingBalance
                                            )
                                            .returning('balance')
                                            .then((bl) => {
                                                res.json({
                                                    result: {
                                                        id: data[0],
                                                        balance: bl[0].balance
                                                    },
                                                    success: true,
                                                    message:
                                                        // '[SERVER] PAYMENT SUCCESSFUL, SEATS ARE PLACED'
                                                        'Thanh toán và mua vé thành công!'
                                                });
                                            });
                                    }
                                );
                                console.log('[SERVER] INSERTED DATA', req.body);
                            })
                            .catch((err) => {
                                console.log(err);
                            });
                    } else {
                        res.json({
                            success: false,
                            message:
                                // '[SERVER] SEATS ARE NOT AVAILABLE!'
                                'Ghế bạn chọn đã có người mua!'
                        });
                    }
                });
        } else {
            res.json({
                success: false,
                message:
                    // '[SERVER] PLEASE CHOOSE YOUR SEAT!'
                    'Vui lòng chọn chỗ ngồi!'
            });
        }
    };
};

const handlePaymentTopup = (req, res, knex, auth) => {
    const userInfo = auth.decodeToken(req.headers.authorization);
    knex.select('balance')
        .from('users')
        .where('id', userInfo.id)
        .then((db) => {
            const afterTopUpBalance = db[0].balance + req.body.amount;
            knex('users')
                .where('id', userInfo.id)
                .update('balance', afterTopUpBalance)
                .returning('balance')
                .then((data) => {
                    res.json({
                        result: data[0],
                        success: true,
                        message: '[SERVER] TOP-UP SUCCESSFUL!'
                    });
                });
        });
};

module.exports = {
    handlePaymentBooking,
    handlePaymentTopup
};
