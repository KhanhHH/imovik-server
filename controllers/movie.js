const handleMovieList = (req, res, knex) => {
    knex.select()
        .from('movies')
        .orderBy('release', 'desc')
        .then((data) => {
            console.log('[SERVER] SENT DATA');
            res.status(200).send(data);
        })
        .catch((err) => {
            console.log(err);
        });
};
const handleMovieCreate = (req, res, knex) => {
    knex('movies')
        .insert(req.body)
        .then(() => {
            res.status(200).send('[SERVER] CREATED MOVIE');
            console.log('[SERVER] INSERTED DATA');
            console.log(req.body);
        })
        .catch((err) => {
            console.log(err);
        });
};

const handleMovieDetail = (req, res, knex) => {
    knex.select()
        .from('movies')
        .where({
            film_id: req.params.id
        })
        .then((data) => {
            console.log('[SERVER] SENT DATA: ', data[0]);
            console.log(req.params);
            res.status(200).send(data[0]);
        })
        .catch((err) => {
            console.log(err);
        });
};


module.exports = {
    handleMovieList,
    handleMovieCreate,
    handleMovieDetail
};
