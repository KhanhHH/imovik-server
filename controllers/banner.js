const handleBanner = (req, res, knex) => {
    knex.select()
        .from('banners')
        .then((data) => {
            res.status(200).json(data);
            console.log(data);
        })
        .catch((err) => {
            console.log(err);
        });

};

module.exports = {
    handleBanner
};
