const { of, from, forkJoin, zip } = require('rxjs');
const {
    mergeMap,
    map,
    tap,
    filter,
    mergeAll,
    pluck,
    toArray,
    combineAll,
    concat,
    distinct,
    groupBy,
    concatAll
} = require('rxjs/operators');

// Fetch 3rd-party API
const api = require('./api');

// CRUD Database
const {
    knex,
    createMovieOnDatabase,
    getMovieListFromDatabase,
    getCinemaListFromDatabase,
    createScheduleOnDatabase,
    clearScheduleOnDatabase,
    updateMovieStatus,
    createBannerOnDatabase,
    clearBannerOnDatabase
} = require('./knex');

// 'res' stand for respond from api
// 'data' stand for respond from database, otherwise it stand for callback data

const updateMovieOnDatabase = () => {
    console.log('[SERVER] Updating movie ... ');
    const allMovieList$ = forkJoin(
        api.fetchPremieringFilmList(),
        api.fetchUpcomingFilmList()
    ).pipe(
        // Get all premiering & upcoming movies data then merge all of them to one
        map(([sourceA, sourceB]) => [
            ...sourceA.response.result,
            ...sourceB.response.result
        ]),
        mergeAll(),
        // Using all film_id to request all movies details
        mergeMap((data) => api.fetchMovieDetail(data.film_id)),
        map((data) => of(data.response.result)),
        concatAll()
    );

    // Update new films and close films is not premierming  anymore.
    allMovieList$
        .pipe(
            // Concat film's data from api and database
            concat(from(getMovieListFromDatabase()).pipe(mergeAll())),
            // Filter new film
            groupBy((data) => data.film_id),
            mergeMap((group) => group.pipe(toArray())),
            filter((group) => group.length === 1),
            mergeAll()
        )
        .subscribe((res) => {
            console.log('[SERVER] Fetched a new film: ', res.film_name);
            // If the flim from database is not in the film list fecthed from api, change it to closed status
            if (res.id) {
                // res.id is to check that film's object come from DB, not from API
                updateMovieStatus(res.film_id, 'closed');
                console.log(
                    '[SERVER] Closed a stopped premiering film: ',
                    res.film_id
                );
            } else {
                const new_film = {
                    film_id: res.film_id,
                    title: res.film_name,
                    description: res.film_description_web_short,
                    classify: res.pg_rating,
                    director: res.film_director,
                    cast: res.list_actor,
                    genre: res.film_category,
                    release: res.publish_date,
                    running: res.film_duration,
                    poster_img: res.poster,
                    poster_img_landscape: res.poster_landscape,
                    status:
                        res.status_id === 1
                            ? 'upcoming'
                            : res.status_id === 2
                            ? 'premiering'
                            : 'closed'
                };
                createMovieOnDatabase(new_film);
            }
        });

    // Update upcoming movie to premiering movie
    allMovieList$.pipe().subscribe((res) => {
        from(getMovieListFromDatabase())
            .pipe(mergeAll())
            .subscribe((data) => {
                if (res.film_id === data.film_id) {
                    // Checking upcoming movie has changed it's status to premiere or not, if it does then update movie status in DB
                    if (res.status_id == '2' && data.status == 'upcoming') {
                        updateMovieStatus(data.film_id, 'premiering');
                    }
                }
            });
    });
};

const fetchAllMovieSchedule = () => {
    clearScheduleOnDatabase().then(() => {
        console.log('[SERVER] Updating schedule ... ');
        from(getMovieListFromDatabase())
            .pipe(
                mergeAll(),
                // Filter premiering film
                filter((data) => data.status === 'premiering'),
                map((data) => data.film_id),
                // Fetch all film schedules
                mergeMap((data) => getFilmScheduleDetail(data)),
                // Filter available schedules
                filter((data) => data.length > 0),
                mergeAll(),
                mergeAll()
            )
            .subscribe((data) => {
                if (data.time) {
                    data.time.map((t_data) => {
                        const new_schedule = {
                            film_id: data.film_id,
                            theater_id: data.theater_id,
                            theater_name: data.theater_name,
                            cinema_group: data.cinema_group,
                            date: data.date,
                            time_id: t_data.session_id,
                            time: t_data.session_time
                                .split(' ')[1]
                                .substring(0, 5)
                        };
                        createScheduleOnDatabase(new_schedule);
                    });
                }
            });
    });
};

const getFilmScheduleDetail = (film_id) => {
    return api.fetchFilmScheduleDetail(film_id).pipe(
        map((data) => from(data.response.result)),
        mergeAll(),
        mergeMap((data) => Object.keys(data).map((key) => data[key])),
        map((data) =>
            from(
                Object.keys(data.cinemas).map((key) => {
                    return {
                        film_id: film_id,
                        theater_id: data.cinemas[key].cinema_id,
                        theater_name: data.cinemas[key].cinema_name,
                        date: data.cinemas[key].versions['2_0']
                            ? data.cinemas[key].versions[
                                  '2_0'
                              ][0].session_time.split(' ')[0]
                            : '',
                        time: data.cinemas[key].versions['2_0'],
                        cinema_group:
                            data.p_cinema_id === '1'
                                ? 'lotte'
                                : data.p_cinema_id === '2'
                                ? 'gc'
                                : data.p_cinema_id === '4'
                                ? 'bhd'
                                : 'other'
                    };
                })
            )
        ),
        mergeAll(),
        groupBy((data) => data.theater_id),
        mergeMap((group) => group.pipe(toArray())),
        toArray()
    );
};

const updateBannerListOnDatabase = () => {
    clearBannerOnDatabase().then(() => {
        api.fetchBannerList().subscribe((res) => {
            from(res.response.result)
                .pipe(
                    toArray(),
                    map((data) => {
                        return from(Object.keys(data).map((key) => data[key]));
                    }),
                    mergeAll(),
                    map((data) => {
                        return {
                            banner_id: data.banner_id,
                            banner_url: data.banner_url
                        };
                    })
                )
                .subscribe((data) => {
                    createBannerOnDatabase(data);
                });
        });
    });
};

const frequencyUpdate = () => {
    updateBannerListOnDatabase();
    updateMovieOnDatabase();
    // Timer log
    setInterval(() => {
        console.log(
            '[' + new Date().toLocaleString() + ']' + '[Server] Running ...'
        );
    }, 5000);

    // Update movies everyday
    setInterval(() => {
        console.log('[Server] Executing Frequency Movies Update ...');
        updateMovieOnDatabase();
    }, 86400000);

    // Update banners everyday
    setInterval(() => {
        console.log('[Server] Executing Frequency Banners Update ...');
        updateBannerListOnDatabase();
    }, 86400000);

    // Update schedules every 5 minutes
    setInterval(() => {
        console.log('[Server] Executing Frequency Schedules Update ...');
        fetchAllMovieSchedule();
    }, 300000);
};

const init = frequencyUpdate();
