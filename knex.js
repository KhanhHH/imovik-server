const knex = require('knex')({
    client: 'pg',
    version: '4.2',
    connection: {
        // host: '127.0.0.1',
        host: 'imovik-db.postgres.database.azure.com',
        user: 'cero@imovik-db',
        password: '',
        database: 'imovik'
    }
});
const createAccountOnDatabase = (account) => {
    const new_account = {
        display_name: account.display_name,
        email: account.email,
        password: account.password,
        balance: account.balance
    };
    return knex('users').insert(new_account);
};

const getAccountInfoFromDatabase = (account_email) => {
    return knex
        .select()
        .from('users')
        .where('email', account_email);
};

const createMovieOnDatabase = (new_film) => {
    knex('movies')
        .insert(new_film)
        .then(() => {
            console.log('[DATABASE] Inserted a new movie: ', new_film);
        })
        .catch((err) => {
            console.log(err);
        });
};

const getMovieListFromDatabase = () => {
    return knex.select().from('movies');
};

const getCinemaListFromDatabase = () => {
    return knex.select().from('cinemas');
};

const createScheduleOnDatabase = (new_schedule) => {
    knex('schedules')
        .insert(new_schedule)
        .then(() => {
            console.log('[DATABASE] Inserted a new schedule: ', new_schedule);
        })
        .catch((err) => {
            console.log(err);
        });
};

const clearScheduleOnDatabase = () => {
    return knex('schedules')
        .del()
        .then(() => {
            console.log('[DATABASE] Cleared all schedules');
        })
        .catch((err) => {
            console.log(err);
        });
};

const updateMovieStatus = (film_id, status) => {
    knex('movies')
        .where('film_id', '=', film_id)
        .update({
            status: status
        })
        .then(() => {
            console.log(
                '[DATABASE] Changed movie status ' + film_id + ' to ' + status
            );
        })
        .catch((err) => {
            console.log(err);
        });
};

const createBannerOnDatabase = (banner) => {
    knex('banners')
        .insert(banner)
        .then(() => {
            console.log('[DATABASE] Inserted a new banner: ', banner);
        })
        .catch((err) => {
            console.log(err);
        });

};
const clearBannerOnDatabase = () => {
    return knex('banners')
        .del()
        .then(() => {
            console.log('[DATABASE] Cleared all banners');
        })
        .catch((err) => {
            console.log(err);
        });
};

exports.knex = knex;

// Account
exports.createAccountOnDatabase = createAccountOnDatabase;
exports.getAccountInfoFromDatabase = getAccountInfoFromDatabase;

// Movies
exports.createMovieOnDatabase = createMovieOnDatabase;
exports.getMovieListFromDatabase = getMovieListFromDatabase;
exports.updateMovieStatus = updateMovieStatus;

//Cinemas
exports.getCinemaListFromDatabase = getCinemaListFromDatabase;

//Schedules
exports.createScheduleOnDatabase = createScheduleOnDatabase;
exports.clearScheduleOnDatabase = clearScheduleOnDatabase;

// Banners
exports.createBannerOnDatabase = createBannerOnDatabase;
exports.clearBannerOnDatabase = clearBannerOnDatabase;
